class NameList {
    var namesDict: [Character: [String]] = [:]
    
    func addName(name: String) {
        guard let litera = name.first else {
            print("Enter name")
            return
        }
        if var names = namesDict[litera] {
            names.append(name)
            names.sort()
            namesDict[litera] = names
        } else {
            namesDict[litera] = [name]
        }
        
    }
    
    func printNames() {
        for key in namesDict.keys.sorted() {
            print(key)
            if let names = namesDict[key] {
                for name in names {
                    print(name)
                }
            }
        }
    }
}

let list = NameList()
list.addName(name: "Акулова")
list.addName(name: "Валерьянов")
list.addName(name: "Бочкин")
list.addName(name: "Бабочкина")
list.addName(name: "Арбузов")
list.addName(name: "Васильева")
list.printNames()
