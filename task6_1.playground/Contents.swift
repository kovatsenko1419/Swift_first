protocol CandidateFilter {
    func filterCandidates(candidates: [Candidate]) -> [Candidate]
}

struct Candidate {
    enum Grade {
        case junior
        case middle
        case senior
    }
     
    let grade: Grade
    let requiredSalary: Int
    let fullName: String
}

extension Candidate {
    struct GradeFilter: CandidateFilter {
        let grade: Grade
        
        func filterCandidates(candidates: [Candidate]) -> [Candidate] {
            return candidates.filter { $0.grade == grade }
        }
    }
    
    struct SalaryFilter: CandidateFilter {
        let salary: Int
        
        func filterCandidates(candidates: [Candidate]) -> [Candidate] {
            return candidates.filter { $0.requiredSalary <= salary }
        }
    }
    
    struct NameFilter: CandidateFilter {
        let name: String
        
        func filterCandidates(candidates: [Candidate]) -> [Candidate] {
            return candidates.filter { $0.fullName.contains(name) }
        }
    }
}
