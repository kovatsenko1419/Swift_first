import UIKit

//Task 1
let milkmanPhrase = "Молоко - это полезно"
print(milkmanPhrase)

//Task2
//let milkPrice: Int = 3

//Task 3
var milkPrice: Double = 3.0
milkPrice = 4.20

//Task 4
var milkBottleCount: Double? = 20
var profit: Double = 0.0
if let milkBottleCount { //Force unwrap может привести к ошибке
    profit = milkPrice * milkBottleCount
    print("Profit: ", profit)
} else {
    print("No bottles – no profit")
}

//Task 5
var employeesList: [String] = []
employeesList.append(contentsOf: ["Иван", "Геннадий", "Петр", "Андрей", "Марфа"])

//Task 6
let workingHours = 39
let isEveryoneWorkHard = workingHours >= 40

print(isEveryoneWorkHard)
