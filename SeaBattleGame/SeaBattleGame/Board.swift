final class Board {
    let size: Int
    var field: [[String]]
    
    init(size: Int) {
        self.size = size
        self.field = Array(repeating: Array(repeating: emptyFieldCell, count: size), count: size)
    }
    
    func printBoard() {
        print("\n")
        for col in 0...size {
            print(col, terminator: "\t")
        }
        var i = 0
        for row in field {
            print("\n")
            print(Character(UnicodeScalar(65 + i)!), terminator: "\t")
            for element in row {
                print(element, terminator: "\t")
            }
            i += 1
        }
        print("\n")
    }
    
    func placeShip(_ ship: Ship) -> Bool {
        print("\nPlace \(ship.size)-decked ship on board\n")
        if ship.size > 1 {
            print("Enter start and end coordinates like \"A1-A2\":")
        } else {
            print("Enter coordinate like \"A1\":")
        }
        let coordinates = readNonEmptyLine().uppercased()
        switch (validateCoordinates(coordinates, field)) {
            
        case .invalid:
            print("Invalid coordinates, try again:\n")
            printBoard()
            return false
            
        case .touching:
            print("Ships should not touch each other, try again\n")
            printBoard()
            return false
            
        case .valid(let parsedCoordinates):
            let (x1, y1) = parsedCoordinates.start
            let (x2, y2) = parsedCoordinates.end
            field[x1][y1] = filledFieldCell
            field[x2][y2] = filledFieldCell
            ship.registrate(x1, y1, x2, y2)
            printBoard()
            return true
        }
    }
    
    func resetBoard() {
        field = Array(repeating: Array(repeating: emptyFieldCell, count: size), count: size)
    }
    
    func validateCoordinates(_ coordinates: String, _ gameBoard: [[String]], isHit: Bool = false) -> CoordinateValidationResult {
        guard let parsedCoordinates = parseCoordinates(coordinates, isHit: isHit) else {
            return .invalid
        }
        
        let (x1, y1) = parsedCoordinates.start
        let (x2, y2) = parsedCoordinates.end
        
        let boardSize = gameBoard.count
        if x1 < 0 || x1 >= boardSize || y1 < 0 || y1 >= boardSize || x2 < 0 || x2 >= boardSize || y2 < 0 || y2 >= boardSize {
            return .invalid
        }
        
            if (x1 == x2 && abs(y1 - y2) == 1 ) || (y1 == y2 && abs(x1 - x2) == 1) || (x1 == x2 && y1 == y2) {
                // Проверка, что корабли не касаются друг друга
                for y in max(0, min(y1, y2) - 1)...min(boardSize - 1, max(y1, y2) + 1) {
                    for x in max(0, min(x1, x2) - 1)...min(boardSize - 1, max(x1, x2) + 1) {
                        if gameBoard[x][y] == filledFieldCell {
                            return .touching
                        }
                    }
                }
                return .valid(parsedCoordinates)
            }
        
        
        return .valid(parsedCoordinates)
    }
    
    private func parseCoordinates(_ input: String, isHit: Bool = false) -> (start: (Int, Int), end: (Int, Int))? {
        
        let parts = input.split(separator: "-")
        
        let coord1 = String(parts[0])
        var coord2 = ""
        
        if parts.count == 2 {
            if isHit {
                return nil
            }
            coord2 = String(parts[1])
        } else if parts.count > 2 {
            return nil
        }
        
        guard let letter1 = coord1.first, let digit1 = Int(coord1.dropFirst()),
              letter1.isLetter, digit1 > 0 else {
            return nil
        }
        
        let x1 = Int(letter1.asciiValue! - 65)
        let y1 = digit1 - 1
        
        var x2 = x1
        var y2 = y1
        if !coord2.isEmpty {
            guard let letter2 = coord2.first, let digit2 = Int(coord2.dropFirst()),
                  letter2.isLetter, digit2 > 0 else {
                return nil
            }
            x2 = Int(letter2.asciiValue! - 65)
            y2 = digit2 - 1
            
            if letter1 == letter2 && digit1 == digit2 {
                return nil
            }
        }
        
        return (start: (x1, y1), end: (x2, y2))
    }
    
    enum CoordinateValidationResult {
        case invalid
        case touching
        case valid((start: (Int, Int), end: (Int, Int)))
    }
}
