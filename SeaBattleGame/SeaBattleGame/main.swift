final class SeaBattleGame {
    
    private let player1: Player
    private let player2: Player
    
    init() {
        print("Enter first player name:")
        self.player1 = Player(name: readNonEmptyLine())
        
        print("Enter second player name:")
        self.player2 = Player(name: readNonEmptyLine())
    }
    
    func start() {
        player1.fillBoard()
        player2.fillBoard()
        
        var currentPlayer = player1
        var currentOpponent = player2
        
        while true {
            print("\n\(currentPlayer.name) move:")
            let successedMove = currentPlayer.makeMove(opponent: currentOpponent)
            if !successedMove {
                let temp = currentPlayer
                currentPlayer = currentOpponent
                currentOpponent = temp
            }
            
            if currentOpponent.checkGameOver() {
                print("\(currentPlayer.name) wins!")
                break
            }
        }
    }
}

let game = SeaBattleGame()
game.start()
