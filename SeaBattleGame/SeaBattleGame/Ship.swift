final class Ship {
    let size: Int
    var isSunk: Bool
    var coordinates: (start: (Int, Int), end: (Int, Int))
    
    init(size: Int) {
        self.size = size
        self.isSunk = false
        self.coordinates = (start: (-1, -1), end: (-1, -1))
    }
    
    func registrate(_ x1: Int, _ y1: Int, _ x2: Int, _ y2: Int) {
        self.coordinates = (start: (x1, y1), end: (x2, y2))
    }
    
    func takeHit(_ x: Int, _ y: Int) {
        if coordinates.start == (x, y) {
            coordinates.start = (-1, -1)
        }
        if coordinates.end == (x, y) {
            coordinates.end = (-1, -1)
        }
        handleSunk()
    }
    
    private func handleSunk() {
        if coordinates.start == (-1, -1) && coordinates.end == (-1, -1) {
            isSunk = true
        }
    }
}
