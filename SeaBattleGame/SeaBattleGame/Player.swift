struct Player {
    let name: String
    var ships: [Ship]
    var board: Board
    var opponentBoard: Board
    
    init(name: String) {
        self.name = name
        self.ships = [Ship(size: 2), Ship(size: 1), Ship(size: 1)]
        self.board = Board(size: 4)
        self.opponentBoard = Board(size: 4)
    }
    
    func checkGameOver() -> Bool {
        ships.allSatisfy { $0.isSunk }
    }
    
    func fillBoard() {
        print("\(name), your board:\n")
        board.printBoard()
        for ship in ships {
            while(true) {
                let shipPlaced = board.placeShip(ship)
                if (shipPlaced) {
                    break
                }
            }
        }
        print("\nYour board looks this:\n")
        board.printBoard()
        print("\nIf you ready type: Y, for reset board type: N\n")
        
        var isBoardReady = false
        while !isBoardReady {
            let decision = readNonEmptyLine()
            switch decision.uppercased() {
            case "Y":
                print("\(name)'s board is setted\n")
                isBoardReady = true
            case "N":
                isBoardReady = true
                board.resetBoard()
                fillBoard()
            default: print("Type Y or N\n")
            }
        }
    }
    
    func makeMove(opponent: Player) -> Bool {
        opponentBoard.printBoard()
        print("\n\(name), enter coordinates to hit:\n")
        let coordinates = readNonEmptyLine().uppercased()
        switch board.validateCoordinates(coordinates, opponentBoard.field, isHit: true) {
        case .invalid:
            print("Invalid coordinates, try again:\n")
            return makeMove(opponent: opponent)
        case .valid(let parsedCoordinates):
            let (x, y) = parsedCoordinates.start
            return checkHit(x, y, opponent)
        default:
            return false
        }
    }
    
    private func checkHit(_ x: Int, _ y: Int, _ opponent: Player) -> Bool {
        if opponent.board.field[x][y] == filledFieldCell {
            print("\nThere is a hit!\n")
            opponent.board.field[x][y] = hittedShipCell
            opponentBoard.field[x][y] = hittedShipCell
            for ship in opponent.ships {
                if !ship.isSunk {
                    ship.takeHit(x, y)
                    if ship.isSunk {
                        print("The ship is sunk!\n")
                        break
                    }
                }
            }
            return true
            
        } else if opponent.board.field[x][y] == emptyFieldCell {
            print("Missed!\n")
            opponentBoard.field[x][y] = missedShotCell
            opponentBoard.printBoard()
            return false
        } else {
            print("You've already shot there\n")
            return true
        }
    }
}
