func readNonEmptyLine() -> String {
    var input: String = ""
    while true {
        if let line = readLine(), !line.isEmpty {
            input = line
            break
        } else {
            print("Please enter a non-empty string:")
        }
    }
    return input
}
