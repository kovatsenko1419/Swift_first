import Foundation

func findIndexWithGenerics<T: Equatable>(of valueToFind: T, in array: [T]) -> Int? {
    for (index, value) in array.enumerated() {
        if value == valueToFind {
            return index
        }
    }
    return nil
}

let arrayOfString = ["кот", "пес", "лама", "попугай", "черепаха"]
let arrayOfDoubles = [0.99, 1.2, 5125.1, 66, 14.001]
let arrayOfInts = [1, 8, 3, 63, 37]

if let foundIndex = findIndexWithGenerics(of: "попугай", in: arrayOfString) {
    print("Попугай's index: \(foundIndex)")
}

if let foundIndex = findIndexWithGenerics(of: 14.001, in: arrayOfDoubles) {
    print("14.001's index: \(foundIndex)")
}

if let foundIndex = findIndexWithGenerics(of: 1, in: arrayOfInts) {
    print("1's index: \(foundIndex)")
}
