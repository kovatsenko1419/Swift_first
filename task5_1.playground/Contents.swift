import Foundation

class Shape {
    let shapeName: String
    
    init(shapeName: String) {
        self.shapeName = shapeName
    }
    
    func calculateArea() -> Double {
         fatalError("not implemented")
    }
    
    func calculatePerimeter() -> Double {
        fatalError("not implemented")
    }
    
}

class Rectangle: Shape {
    private let height: Double
    private let width: Double
    
    init(shapeName: String, height: Double, width: Double) {
        self.height = height
        self.width = width
        super.init(shapeName: shapeName)
    }
    
    override func calculateArea() -> Double {
        height * width
    }
    
    override func calculatePerimeter() -> Double {
        2 * (height + width)
    }
}

class Circle: Shape {
    private let radius: Double
    
    init(shapeName: String, radius: Double) {
        self.radius = radius
        super.init(shapeName: shapeName)
    }
    
    override func calculateArea() -> Double {
        Double.pi * pow(radius, 2)
    }
    
    override func calculatePerimeter() -> Double {
        2 * Double.pi * radius
    }
}

class Square: Shape {
    private let side: Double
    
    init(shapeName: String, side: Double) {
        self.side = side
        super.init(shapeName: shapeName)
    }
    
    override func calculateArea() -> Double {
        pow(side, 2)
    }
    
    override func calculatePerimeter() -> Double {
        4 * side
    }
}

let shapes: [Shape] = [
    Circle(shapeName: "Circle", radius: 10.5),
    Rectangle(shapeName: "Rectangle", height: 4.5, width: 7.1),
    Square(shapeName: "Square", side: 9.9)
]

var area: Double
var perimeter: Double

for shape in shapes {
    area = shape.calculateArea()
    perimeter = shape.calculatePerimeter()
    print("\(shape.shapeName) area: ", area)
    print("\(shape.shapeName) perimeter: ", perimeter)
}
