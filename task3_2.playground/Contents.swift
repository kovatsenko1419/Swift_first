import UIKit

func checkPrimeNumber(value: Int) -> Bool {
    switch value {
    case 0...1: return false
    case 2: return true
    default: return needCheck(value: value)
    }
    
    func needCheck(value: Int) -> Bool {
        let sqrtValue = Int(sqrt(Double(value)))
        for i in 2...sqrtValue {
            if value % i == 0 {
                return false
            }
        }
        return true
    }
}
print(checkPrimeNumber(value: 7))
print(checkPrimeNumber(value: 8))
print(checkPrimeNumber(value: 13))
print(checkPrimeNumber(value: 14))
print(checkPrimeNumber(value: 15))
print(checkPrimeNumber(value: 1))
print(checkPrimeNumber(value: 0))
print(checkPrimeNumber(value: 2))
