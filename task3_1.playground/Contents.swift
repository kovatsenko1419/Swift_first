import UIKit

func makeBuffer() -> (String) -> Void {
    var bufferResult = ""
    
    func bufferMaker(value: String) {
        value.isEmpty ? print(bufferResult) : bufferResult.append(value) 
    }
    
    return bufferMaker
}

var buffer = makeBuffer()
buffer("Замыкание")
buffer(" использовать")
buffer(" нужно!")
buffer("")
